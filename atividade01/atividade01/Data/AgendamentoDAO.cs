﻿using atividade01.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace atividade01.Data
{
    class AgendamentoDAO
    {
        readonly SQLiteConnection Conexao;


        public AgendamentoDAO(SQLiteConnection conexao) //agendamentodao eh database puxando dados
        {
            this.Conexao = conexao;// fazendo link da classe que esta recebendo dadoss da variavel conexao
            this.Conexao.CreateTable<Agendamento>();//criado uma tabela 

        }

        public void Salvar(Agendamento agendamento)//funcao de insersao
        {
            Conexao.Insert(agendamento);
        }
    }
}
