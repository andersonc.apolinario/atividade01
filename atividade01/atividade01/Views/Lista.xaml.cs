﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace atividade01.Views
{
    public class Modelo
    {
        public string Marca { get; set; }
        public string Modelo2 { get; set; }
        public float Preco { get; set; }

        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }
    }
    public partial class Lista : ContentPage
    {
        public List<Modelo> Modelos { get; set; }
        public Lista()
        {
            InitializeComponent();

            this.Modelos = new List<Modelo>
            {
                new Modelo { Marca = "Chevrolet", Modelo2 = "Onix", Preco = 44000 },
                new Modelo { Marca = "Hyundai", Modelo2 = "Hb20", Preco = 42000 },
                new Modelo { Marca = "Renault", Modelo2 = "Sandero", Preco = 39000},
                new Modelo { Marca = "Ford", Modelo2 = "Fiesta", Preco = 29000 },
                new Modelo { Marca = "Honda", Modelo2 = "Civic", Preco = 84000 }
            };

            this.BindingContext = this;
        }
        private void ListViewModelos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var modelo = (Modelo)e.Item;

            Navigation.PushAsync(new DescricaoView(modelo));

            //DisplayAlert("Serviço", string.Format("" +
            //    "Você selecionou o serviço '{0}'. Valor {1}",
            //    modelo.Marca, modelo.Modelo2), "OK");

        }
    }
}
