﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace atividade01.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoView  : ContentPage
	{
        private const float VIDROSELETRICOS = 545;
        private const float TRAVASELETRICAS = 260;
        private const float ARCONDICIONADO = 480;
        private const float CAMERADERE = 180;
        private const float CAMBIO = 460;
        private const float SUSPENSAO = 360;
        private const float FREIOS= 250;

        public string TextoVidrosEletricos         

        {
            get
            {
                return string
                 .Format("VidrosEletricos - R$ {0}", VIDROSELETRICOS);
            }


            
        }
        public string TextoTravasEletricas

        {
            get
            {
                return string
                 .Format("TravasEletricas - R$ {0}", TRAVASELETRICAS);
            }



        }
        public string TextoArCondicionado

        {
            get
            {
                return string
                 .Format("TextoArCondicionado - R$ {0}", ARCONDICIONADO);
            }



        }
        public string TextoCameradeRé

        {
            get
            {
                return string
                 .Format("TextoCameradeRé - R$ {0}", CAMERADERE);
            }



        }
        public string TextoCambio

        {
            get
            {
                return string
                 .Format("TextoCambio - R$ {0}", CAMERADERE);
            }



        }

        public string TextoSuspensao

        {
            get
            {
                return string
                 .Format("TextoSuspensao - R$ {0}", SUSPENSAO);
            }



        }
        public string TextoFreios

        {
            get
            {
                return string
                 .Format("TextoFreios - R$ {0}", FREIOS);
            }



        }

        public string ValorTotal//criado uma proprieadade para valor total, ser exibido no celular parte inferior
        {
            get
            {
                return string.Format("Valor total: R$ {0}", Modelo.Preco// valor total ta chamando a viw la no Descricao view xaml, la tem o binding dele, criado aki primeiro e depois o binding dele
                + (IncluiVidrosEletricos ? VIDROSELETRICOS : 0)
                + (IncluiTravasEletricas ? TRAVASELETRICAS : 0)
                + (IncluiArCondicionado ? ARCONDICIONADO : 0)
                + (IncluiCameradeRé ? CAMERADERE : 0)
                + (IncluiCambio ? CAMBIO : 0)
                + (IncluiSuspensao ? SUSPENSAO : 0)
                + (IncluiFreios ? FREIOS : 0)
                );
            }
        }

        bool incluiVidrosEletricos; //criando uma variavel incluiTransporte 
        public bool IncluiVidrosEletricos//propriedade
        {
            get
            {
                return incluiVidrosEletricos;
            }
            set
            {
                incluiVidrosEletricos = value;
                OnPropertyChanged(nameof(ValorTotal)); //acrescenta valor e chama tanto view quanto xamel
                ////if (incluiTransporte)
                //    DisplayAlert("transporte", "ativo", "ok");
                //else
                //    DisplayAlert("transporte", "inativo", "ok");
            }
        }

        bool incluiTravasEletricas; //criando uma variavel incluiTransporte 
        public bool IncluiTravasEletricas//propriedade
        {
            get
            {
                return incluiTravasEletricas;
            }
            set
            {
                incluiTravasEletricas = value;
                OnPropertyChanged(nameof(ValorTotal)); //acrescenta valor e chama tanto view quanto xamel
                ////if (incluiTransporte)
                //    DisplayAlert("transporte", "ativo", "ok");
                //else
                //    DisplayAlert("transporte", "inativo", "ok");
            }
        }

        bool incluiArCondicionado; //criando uma variavel incluiTransporte 
        public bool IncluiArCondicionado//propriedade
        {
            get
            {
                return incluiArCondicionado;
            }
            set
            {
                incluiArCondicionado = value;
                OnPropertyChanged(nameof(ValorTotal)); //acrescenta valor e chama tanto view quanto xamel
                ////if (incluiTransporte)
                //    DisplayAlert("transporte", "ativo", "ok");
                //else
                //    DisplayAlert("transporte", "inativo", "ok");
            }
        }
        bool incluiCameradeRé; //criando uma variavel incluiTransporte 
        public bool IncluiCameradeRé//propriedade
        {
            get
            {
                return incluiCameradeRé;
            }
            set
            {
                incluiCameradeRé = value;
                OnPropertyChanged(nameof(ValorTotal)); //acrescenta valor e chama tanto view quanto xamel
                ////if (incluiTransporte)
                //    DisplayAlert("transporte", "ativo", "ok");
                //else
                //    DisplayAlert("transporte", "inativo", "ok");
            }
        }

        bool incluiCambio; //criando uma variavel incluiTransporte 
        public bool IncluiCambio//propriedade
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                OnPropertyChanged(nameof(ValorTotal)); //acrescenta valor e chama tanto view quanto xamel
                ////if (incluiTransporte)
                //    DisplayAlert("transporte", "ativo", "ok");
                //else
                //    DisplayAlert("transporte", "inativo", "ok");
            }
        }

        bool incluiSuspensao; //criando uma variavel incluiTransporte 
        public bool IncluiSuspensao//propriedade
        {
            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
                OnPropertyChanged(nameof(ValorTotal)); //acrescenta valor e chama tanto view quanto xamel
                ////if (incluiTransporte)
                //    DisplayAlert("transporte", "ativo", "ok");
                //else
                //    DisplayAlert("transporte", "inativo", "ok");
            }
        }

        bool incluiFreios; //criando uma variavel incluiTransporte 
        public bool IncluiFreios//propriedade
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal)); //acrescenta valor e chama tanto view quanto xamel
                ////if (incluiTransporte)
                //    DisplayAlert("transporte", "ativo", "ok");
                //else
                //    DisplayAlert("transporte", "inativo", "ok");
            }
        }




        private Modelo Modelo { get; set; }


        public DescricaoView(Modelo modelo)
		{
			InitializeComponent ();
            this.Title = modelo.Modelo2;            
            this.Modelo = modelo;
            this.BindingContext = this;
        }

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView(this.Modelo));/*  "this " exiba esta Propriedade*/
        }


    }
}