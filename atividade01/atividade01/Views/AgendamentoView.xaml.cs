﻿using atividade01.Data;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace atividade01.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AgendamentoView : ContentPage
	{
		public AgendamentoView (Modelo modelo)
		{
			InitializeComponent ();
            
        }

        private void ButtonAgendar_Clicked(object sender, EventArgs e)
        {

           
            
                Permissao();
            
           
        }

        public async void Permissao()// funcao que requisita a funcao de acesso a base
        {
            try
            {
                var status = await
                CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await
                    CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                    {
                        await DisplayAlert("Permissão", "Precisamos da sua permissão para armazenar dados no dispositivo.", "OK");
                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[]
                    { Permission.Storage });
                    status = results[Permission.Storage];
                }
                if (status == PermissionStatus.Granted)
                {
                    await DisplayAlert("Sucesso!", "Prossiga com a operação", "OK");
                    FazerAgendamento();// essa funcao chama o agendamento 
                }
                else if (status != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Atenção", "Para utilizar os serviços de persistência de dados, aceite a permissão solicitada pelo dispositivo.", "OK");
                }
            }
            catch (Exception ex)
            {
                //await DisplayAlert("Erro", "Erro no processo " + ex.Message, "OK");
            }
        }

        public void FazerAgendamento()
        {

            Console.WriteLine("iniciando agendamento");

            using (var conexao = DependencyService.Get<ISQLite>().conexao())
            {
                AgendamentoDAO dao = new AgendamentoDAO(conexao);
                dao.Salvar(new Models.Agendamento("Compra Agendada", "3456", "anderson@email.com"));

                DisplayAlert("sucesso", "agendamento realizado", "ok");
            }

        }
    }
}