﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace atividade01.Models
{
    class Agendamento
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }

        public Agendamento(string nome, string celular, string email)
        {
            this.Nome = nome;
            this.Celular = celular;
            this.Email = email;
        }
    }
}
