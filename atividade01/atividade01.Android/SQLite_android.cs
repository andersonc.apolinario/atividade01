﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using atividade01.Data;
using atividade01.Droid;
using SQLite;

[assembly: Xamarin.Forms// depois de criadno comando return, ali embaixo veio para ca e fez esse comando
    .Dependency(typeof(SQLite_android))]// ficou em vermelho clicaco na luz amarela e dado comando using "injecao de dependencia -"fornce os requisitos para acessar o banco de dados local
namespace atividade01.Droid
{
    class SQLite_android : ISQLite

    {
        private const string arquivoDb = "Atividade01.db3";


        public SQLiteConnection conexao()
        {
            var pathDb = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.Path, arquivoDb);//path inicial ficou em vermelho utilize para importar using
            return new SQLite.SQLiteConnection(pathDb);
        }
    }
}